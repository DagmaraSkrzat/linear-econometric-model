/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekonometriaTest;
import ekonometria.ParamStruc;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;

/**
 *
 * @author dagmaraskrzat
 */
public class ParamStrucTest {
    
        
    @Test
    public void testCountParameters() {
        System.out.println("countParameters");
        double[][] x = {{1, 1, 1, 1}, {2, 0, 3, 4}, {4, 3, 2, 1}, {2, 3, 1, 2}};
        double[] y = {1, 2, 3, 3};
        ParamStruc instance = new ParamStruc();
        double[] expResult = {6.33333333333334, -0.33333333333357, -0.88888888888892, -0.55555555555536};
        double[] result = instance.countParameters(x, y);
        double delta = 0.0000000000001;
        assertArrayEquals(expResult, result, delta);
    }
        
}
