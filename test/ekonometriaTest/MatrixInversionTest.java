/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekonometriaTest;

import ekonometria.MatrixInversion;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author dagmaraskrzat
 */
public class MatrixInversionTest {
    
    MatrixInversion mi = null;
    double[][] x1 = {{1, 1, 1, 1}, {2, 0, 3, 4}, {4, 3, 2, 1}, {2, 3, 1, 2}};
    double[][] x2 = {{-2.333333333333333, 0.3333333333333334, 0.5555555555555556, 0.22222222222222232}, {1.3333333333333333, -0.33333333333333337, -0.22222222222222224, 0.1111111111111111}, {3.333333333333333, -0.3333333333333333, -0.22222222222222224, -0.888888888888889}, {-1.3333333333333333, 0.3333333333333333, -0.11111111111111112, 0.5555555555555556}};
    double[][] x3 = {{1, 1, 1, 1, 1}, {1, 2, 3, 4, 6, 7}, {4, 3, 2, 1}, {2, 3, 1, 2}};
    double[][] x4 = {{1, 1, 1, 1}, {1, 2, 3, 4}, {4, 3, 2, 1}, {2, 3, 1, 2}};
    double[] y = {1, 2, 3, 3};
    
    @Before
    public void setUp(){
        mi = new MatrixInversion();
    }
    
    @Test
    public void testInversOfMatrix() {
       assertArrayEquals(x2,mi.inverseOfMatrix(x1));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void shouldThrownIllegalArgumentExceptionOnWrongParameters() {
        mi.inverseOfMatrix(x3);
    }
    
    @Test(expected = ArithmeticException.class)
    public void shouldThrownArithmeticExceptionOnWrongParameters() {
        mi.inverseOfMatrix(x4);
    }
    
}
