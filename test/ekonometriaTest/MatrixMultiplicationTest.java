/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekonometriaTest;

import ekonometria.MatrixMultiplication;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author dagmaraskrzat
 */
public class MatrixMultiplicationTest {
    
        MatrixMultiplication mm = null;
        double[][] x = {{1, 1, 1, 1, 1}, {2, 0, 3, 4}, {4, 3, 2, 1}, {2, 3, 1, 2}};
        double[][] x1 = {{1, 1, 1, 1}, {2, 0, 3, 4}, {4, 3, 2, 1}, {2, 3, 1, 2}};
        double[][] x2 = {{4, 9, 10, 8}, {9, 29, 18, 15}, {10, 18, 30, 21}, {8, 15, 21, 18}};  //xTx
        double[] y = {1, 2, 3, 3};
        double[] y1 = {1, 2, 2, 3, 3};
        double[] y2 = {1, 2};
        double[] z = {9, 23, 19, 17}; //xTy
        
        @Before
        public void setUp(){
        mm = new MatrixMultiplication();
        }
        
        @Test
        public void testXtX() {
            assertArrayEquals(x2, mm.xTx(x1));
        }
        
        @Test
        public void testXtY() {
            double delta = 0.0000000000001;
            assertArrayEquals(z, mm.xTy(x1, y), delta);
        }
        
        @Test(expected = IllegalArgumentException.class)
        public void shouldThrownIllegalArgumentExceptionOnWrongParameters() {
            mm.xTx(x);                                                          //x is not a square matrix
        }
        @Test(expected = IllegalArgumentException.class)
        public void shouldThrownIllegalArgumentExceptionOnWrongParameters2() {
            mm.xTy(x, y1);                                                      //x[0][] is the same size as y1, but the rest of arrays of x are diffrent size
        }
        @Test(expected = IllegalArgumentException.class)
        public void shouldThrownIllegalArgumentExceptionOnWrongParameters3() {
            mm.xTy(x1, y2);                                                     //all arrays of x are the same size (with each other), but y is not the same size as the size of arrays of x
        }

}
