/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package verificationOfModel;

import ekonometria.DataInsertion;
import ekonometria.ParamStruc;
import java.text.DecimalFormat;

/**
 *
 * @author dagmaraskrzat
 */
public class FittingTheModel {
    //Evaluation of the level of fit of the model to empirical data.
    private DataInsertion di;
    private ParamStruc ps;
    private final double[] yEmpirical = di.getY();
    private final double[] yOfModel = ps.getYOfModel();
    private double coefficientOfDetermination;
    private double coefficientOfConvergence;
    private double residualVariance;
    
    public double getCoefficientOfDetermination() {
        return coefficientOfDetermination;
    }
    
    public double getCoefficientOfConvergence() {
        return coefficientOfConvergence;
    }
    
     
    public void ifModelFits() {
        ArithmeticCalculations ac = new ArithmeticCalculations();
        double av = ac.average(yEmpirical);
        double cd = coeffOfDeterm();
        double cc = 1 - cd;
        double workinVariable; 
        String sb;
        DecimalFormat df = new DecimalFormat("###.##");
        
        if(0 <= cd && cd <= 1 && cd + cc == 1) {
            if(0.9 < cd && cd <= 1) {
                System.out.println("Very good match. Only " + df.format(cc*100) + "% of the data is not explained by the model." );
            } else if (0.8 < cd && cd <= 0.9) {
                System.out.println("Good match." + df.format(cc*100) + "% of the data is not explained by the model.");
            } else if (0.6 < cd && cd <= 0.8) {
                System.out.println("Satisfactory match." + df.format(cc*100) + "% of the data is not explained by the model.");
            } else if (0.5 < cd && cd <= 0.6) {
                System.out.println("Weak match. Too much mismatch, up to" + df.format(cc*100) + "% of the data is not explained by the model.");
            } else if (0 <= cd && cd <= 0.5) {
                System.out.println("Unsatisfactory adjustment, up to" + df.format(cc*100) + "% of the data is not explained by the model.");
            }
        } 
        workinVariable = Math.sqrt((residualVariance));
        sb = df.format(workinVariable);
        System.out.println("The average absolute error we will make using this model is " + sb);
        workinVariable = (residualVariance/av)*100;
        sb = df.format(workinVariable);
        System.out.println("The relative error that we will make using this model is " + sb + "%");
    }
    
    //[sum(y^-yśr)^2/sum(yt-yśr)^2
    public double coeffOfDeterm() {
        ArithmeticCalculations ac = new ArithmeticCalculations();
        double av = ac.average(yEmpirical);
        double[] difference1 = ac.differenceOfMatrixAndNumber(yOfModel, av);
        double[] difference2 = ac.differenceOfMatrixAndNumber(yEmpirical, av);
        coefficientOfDetermination = ac.quadraticSum(difference1) / ac.quadraticSum(difference2);
        if(coefficientOfConvergence >= 0 && coefficientOfConvergence <=1) {
            return coefficientOfDetermination;
       } else {
            throw new ArithmeticException("Coefficient of determination can only take values from 0 to 1.");
       }
    }
    
    //[sum(y-y^)^2/sum(yt-yśr)^2
    public double coeffOfConv() {
       ArithmeticCalculations ac = new ArithmeticCalculations();
       double av = ac.average(yEmpirical);
       double[] difference1 = ac.differenceOfMatrices(yEmpirical, yOfModel);
       double[] difference2 = ac.differenceOfMatrixAndNumber(yEmpirical, av);
       coefficientOfConvergence = ac.quadraticSum(difference1) / ac.quadraticSum(difference2);
       if(coefficientOfConvergence >= 0 && coefficientOfConvergence <=1) {
       return coefficientOfConvergence;
       } else {
            throw new ArithmeticException("Coefficient of convergence can only take values from 0 to 1.");
       }
    }

    public double residualVariance() {
        ArithmeticCalculations ac = new ArithmeticCalculations();
        double[] difference = ac.differenceOfMatrices(yEmpirical, yOfModel);
        residualVariance = ac.quadraticSum(difference) / (difference.length - 1);
        return residualVariance;
    }
}
