/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package verificationOfModel;

/**
 *
 * @author dagmaraskrzat
 */
public class ArithmeticCalculations {
    
    public double average(double[] array) {
        double av;
        av = sum(array) / array.length;
        return av;
    }
    
    public double sum(double[] matrix) {
        double sum = 0;
        for(double m : matrix) {
            sum += m;
        }
        return sum;
    }

    public double quadraticSum(double[] matrix) {
        double sum = 0;
        for(double m : matrix) {
            sum += Math.pow(m, 2);
        }
        return sum;
    }

    public double[] differenceOfMatrices(double[] matrix1, double[] matrix2) {
        
        //matrixes must be the same size
        
        double[] difference = {};
        for(int i = 0; i < matrix1.length; i++) {
           for(int j = 0; j < matrix2.length; j++){
               difference[i] = matrix1[i] - matrix2[j];
           }
        }
        return difference;
    }

    public double[] differenceOfMatrixAndNumber(double[] matrix1, double number) {
        double[] difference = {};
        for(int i = 0; i < matrix1.length; i++) {
               difference[i] = matrix1[i] - number;
        }
        return difference;
    }

}
