/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekonometria;

/**
 *
 * @author dagmaraskrzat
 */
public class MatrixMultiplication {
    //all arrays in x should be the same size (just for the purpose of counting structure parameters)
    public double[][] xTx(double[][] x) {  
        
        for(int i = 1; i < x.length; i++) {
           if (x[i].length != x[i-1].length) {
                throw new IllegalArgumentException("Size of one of the arrays of matrix A is not equal to size of matrix A. Matrix A should be square.");
            } 
        }
        double[][] resoult = new double[x.length][x.length];
        
        for(int j=0; j < x.length; j++) {
            for(int i = 0; i < x.length; i++){
            resoult[j][i] = multiMatrix(x[j], x[i]);
            }
    }
        
        return resoult;
    }
    
    //all arrays in x should be the same length
    //y should be the same size as arrays in x
    //(all for the purpose of counting structure parameters)
    public double[] xTy(double[][] x, double[] y){         
        
        for (double[] x1 : x) {
            if (x1.length != y.length) {
                throw new IllegalArgumentException("Size of one of the arrays of matrix A is not equal to size of matrix A. Matrix A should be square.");
            } 
        }
        double il;
        double[] resoult = new double[x.length]; 
        
        for (int i = 0; i < y.length; i++) {            
            il = multiMatrix(x[i], y);          
            resoult[i] = il;
        }
        
        return resoult;
    }   
    
    private double multiMatrix(double[] mat1, double[] mat2) {
        
        double product, sum = 0;
        
        for(int i=0; i < mat1.length; i++) {
            product = mat1[i] * mat2[i];
            sum += product;
        }
        
        return sum;
    }
}
