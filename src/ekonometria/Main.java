/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekonometria;


/**
 *
 * @author dagmaraskrzat
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DataInsertion di = new DataInsertion();
        double[][] x = di.insertX();
        double[] y = di.insertY();
        ParamStruc ps = new ParamStruc();
        ps.countParameters(x, y);
    }

}
