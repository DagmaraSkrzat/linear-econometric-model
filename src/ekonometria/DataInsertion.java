/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekonometria;

import java.util.Scanner;

/**
 *
 * @author dagmaraskrzat
 */
public class DataInsertion {
    private int numberOfCases;
    private int numberOfVar;
    private final double[][] x;
    private final double[] y;
    
    public DataInsertion() {
        numberOfVar = insertNumbVar(); 
        numberOfCases = insertNumbCases();          
        x = new double[numberOfVar + 1][numberOfCases];
        y = new double[numberOfCases];
    }
   
    public int getNumberOfCases(){
        return numberOfCases;
    }
    
    public int getNumberOfVar(){
        return numberOfVar;
    }
    
    public double[][] getX() {
        return x;
    }
    
    public double[] getY() {
        return y;
    }
    
    ///////////////////////////////////////////////////////////
    
    public double[][] insertX(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Insert " + numberOfCases + " values of " + numberOfVar + " variables.");
            for (int j = 0; j < numberOfVar + 1; j++) {
                System.out.println("Variable X" +  j);
                for (int i = 0; i < numberOfCases; i++) {
                    if(j == 0) {
                        x[j][i] = 1;
                    } else {
                    x[j][i] = sc.nextInt();
                    }
                }
            }
        return x;
        }

    public double[] insertY(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Insert " + numberOfCases + " values of Y.");
                for (int i = 0; i < numberOfCases; i++) {
                    y[i] = sc.nextInt();
                }
        return y;
        }
    /////////////////////////////////////////////////////////////
    
//    public double[][] insertXFile() {
//        
//    }
//    
//    public double[] insertYFile(){
//        
//    }
    
    
    
    private int insertNumbCases(){
        Scanner sc = new Scanner(System.in);
        int number;
        System.out.println("What is the number of cases?");
        number = Integer.parseInt(sc.next());
        numberOfCases = ifCorrectNumber(number);
            return numberOfCases;
}

    private int insertNumbVar() {
        Scanner sc = new Scanner(System.in);
        int number;
        System.out.println("What is the number of var?");
        number = Integer.parseInt(sc.next());
        numberOfVar = ifCorrectNumber(number);
        return numberOfVar;
    }
    
    
    private int ifCorrectNumber(int number) {
        Scanner sc = new Scanner(System.in);
        while (number <= 0) {
            if (number > 0) {
            break;
            }
            if (number < 0) {
                System.out.println("Did you mean " + number * (-1) + "? Y/N");
                if(sc.hasNext("Y")) {
            //if ("Y".equals(sc.nextLine())) {
                    number = number * (-1);
                    break;
                } 
                if ("N".equals(sc.next())) {
                    System.out.println("Please enter correct number of cases.");
                    number = Integer.parseInt(sc.next());
                }
            }
            if (number == 0) {
                System.out.println("The number of cases must be greater than zero. Please enter correct number of cases.");
                    number = Integer.parseInt(sc.next());

            }
         
        } 
        return number;
    }
}