/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekonometria;


/**
 *
 * @author dagmaraskrzat
 */
public class MatrixInversion {

    //algorythm:
    //A * A^(-1) = I
    //A * X = I
    //lowerA * (upperA * X) = I
    //lowerA * Y = I   -> mając Y wracam do obliczenia X: upperA * X = Y
    public double[][] inverseOfMatrix(double[][] matrix) {
        double[][] y = new double[matrix.length][matrix.length];
        double[][] x = new double[matrix.length][matrix.length];
        double[][] pom = lowerUpperMatrix(matrix);

        //L*Y=I -> Y
        for (int j = 0; j < pom.length; j++) {                  //j- number of row
            for (int i = 0; i < pom.length; i++) {              //i - numebr of column
                double il, sum = 0;
                for (int k = 0; k < j; k++) {
                    il = pom[j][k] * y[k][i];
                    sum += il;
                }

                if (i == j) {
                    y[j][i] = 1 - sum;
                } else {
                    y[j][i] = -sum;
                }
            }
        }
        
       

        //U*X=Y -> X
        for (int j = pom.length - 1; j >= 0; j--) {             //j- number of row
            for (int i = 0; i < pom.length; i++) {              //i - number of column
                double il, sum = 0;
                for (int k = j + 1; k < pom.length; k++) {
                    il = pom[j][k] * x[k][i];
                    sum += il;
                }
                x[j][i] = (y[j][i] - sum) / pom[j][j];
                
            }
        }
        return x;
    }

    private double[][] lowerUpperMatrix(double[][] matrixA) {
        int n = matrixA.length;
        double[][] lower = new double[n][n];
        double[][] upper = new double[n][n];
        double sum;
   
        // that restriction is enough in this place, because on the stack the lowerUperperMatrix method will be called in first place (before inverseOfMatrix method)
        for (double[] pom : matrixA) {
            if (pom.length != n) {
                throw new IllegalArgumentException("Size of one of the arrays of matrix A is not equal to size of matrix A. Matrix A should be square.");
            }
        }


        for (int j = 0; j < n; j++) {
            for (int i = 0; i <= j; i++) {
                sum = 0;
                for (int k = 0; k <= i - 1; k++) {
                    sum += lower[i][k] * upper[k][j];
                }
                upper[i][j] = matrixA[i][j] - sum;

                matrixA[i][j] = upper[i][j];

                if (i == j) {
                    if (upper[j][j] == 0) {
                        throw new ArithmeticException("upper[" + j + "][" + j + "] is equal to zero. There will be division by upper[" + j + "][" + j + "]. You can't divide by zero.");
                    }
                    lower[i][j] = 1;
                }
            }
            for (int i = j + 1; i < n; i++) {
                sum = 0;
                for (int k = 0; k < j; k++) {
                    sum += lower[i][k] * upper[k][j];
                }
                lower[i][j] = (matrixA[i][j] - sum) / upper[j][j];

                matrixA[i][j] = lower[i][j];

            }
        }

        return matrixA;
    }

    public String matrixToString(double[][] tab) {
        String tablica = "";
        for (double[] tab1 : tab) {
            for (int i = 0; i < tab.length; i++) {
                tablica = tablica + " / " + tab1[i];
            }
        }
        return tablica;
    }

}
