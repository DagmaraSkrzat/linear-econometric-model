/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ekonometria;

import java.text.DecimalFormat;


/**
 *
 * @author dagmaraskrzat
 */
public class ParamStruc {
    private double[] paramStruc;
    private double[] yOfModel;
    
    public double[] getParamStruc() {
        return paramStruc;
    }
    public double[] getYOfModel() {
        return yOfModel;
    }
    
    public double[] countParameters(double[][] x, double[] y) {        
        MatrixMultiplication matrixMultiplication = new MatrixMultiplication();
        MatrixInversion matrixInversion = new MatrixInversion();
        double[][] m1 = matrixInversion.inverseOfMatrix(matrixMultiplication.xTx(x));
        double[] m2 = matrixMultiplication.xTy(x, y);    
        paramStruc = matrixMultiplication.xTy(m1, m2);
        System.out.println("Your linear model:");
        System.out.println(formOfModel(paramStruc));
        return paramStruc;
    }
    
    //y^(t) = xi(t) * ai(t)
    public double[] yOfModel() {
        DataInsertion di = new DataInsertion();
        double[][] xOfModel = di.getX();
        double sum = 0;
        for (int i = 0; i < di.getNumberOfCases(); i++) {
           for (int j = 0; j < di.getNumberOfVar(); j++) {
               sum += xOfModel[j][i] * paramStruc[j];
           }
           yOfModel[i] = sum;
           sum = 0;
        }
        return yOfModel;
    }

    private StringBuilder formOfModel(double[] mat){
        StringBuilder sb = new StringBuilder();
        DecimalFormat df = new DecimalFormat("###.###");
        sb.append("Y = ");
         for(int i = 0; i < mat.length; i++){
             if(i == 0) { 
                 sb.append(df.format(mat[i]));
             } else {
                 if (mat[i] < 0) {
                    sb.append(df.format(mat[i])).append("*X").append(i);
                 } else if (mat[i] > 0) {
                    sb.append("+").append(df.format(mat[i])).append("*X").append(i);
                 }
             }
        }
        return sb;
    }    
    


}
